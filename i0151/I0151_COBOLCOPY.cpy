       01  HEADER-MAIN.
           05  HEADER-ELEMENT.
               10  BATCH-HDR                   PIC  X(17).
               10  SEQUENCE-1                  PIC  9(10).
               10  RECORD-TYPE-HDR             PIC  X(3).
               10  DOCUMENTODEPAGO             PIC  X(20).
               10  DOCUMENTOPAGADO             PIC  X(20).
               10  CONVERSOR-KEY               PIC  X(14).
               10  TIPO-PAGO                   PIC  X(1).
               10  CHEQUE-LETRA                PIC  9(10).
               10  FECHA-ALTA                  PIC  X(10).
               10  IMPORTE-ORIGINAL            PIC  9(13).
               10  IMPORTE-ORIGINAL-INV        PIC  9(13).
               10  TRANSOURCE                  PIC  X(20).
               10  SUJETO-FACT                 PIC  X(1).
               10  Special-GL-Indicator        PIC  X(1).
               06  WITH-MAIN.
                   10  RECORD-TYPE-WIT         PIC  X(3).
                   10  IMPORTE-RETENCION       PIC  9(13).
                   10  WH-TAX-TYPE             PIC  X(2).
                   10  WH-TAX-CODE             PIC  X(2).
           05  CONTROL-SEGMENT.
               10  CTRL-BATCH-KEY              PIC  X(27).
               10  CTRL-BATCH-NUM              PIC  X(17).
               10  CTRL-INTERFACE-NAME         PIC  X(5).
               10  CTRL-SEQ-NBR                PIC  9(10).
               10  TOT-REC-COUNT               PIC  9(10).
               10  SAP-DISC-AMT-TOT            PIC  X(18).
               10  SAP-RETL-AMT-TOT            PIC  X(18).
               10  SAP-INV-COST-TOT            PIC  X(18).
               10  SAP-CHECK-AMT-TOT           PIC  X(18).
               10  CTRL-FILLER                 PIC  X(859).